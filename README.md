# Plan nauki Dev-Ops

1. Wygeneruj projekt springowy [https://start.spring.io/](https://start.spring.io/)
    - dodaj zależność do 'web'
    - po wygenerowaniu otwórz i odpal projekt w IntelliJ
2. Dodaj stronę główną - `index.html` do folderu `src/main/resources/static`
3. Wygeneruj JAR-a mavenem - `./mvnw clean install` w folderze projektu
    - poczytaj o `mvnw` - Maven Wrapper
    - po zbudowaniu w folderze `target` pojawi się jar z gotową aplikacją, ubij aplikację w IntelliJ i odpal z konsoli `java -jar NAZWA_JARA.jar`
4. Wykup server VPS (Aruba, OVH, Digitaocean - gdzie tańszy), połącz się po ssh do serwera
5. Skonfiguruj serwer - zainstaluj Javę
6. Przekopiuj z laptopa JARa na serwer (`scp`)
7. Uruchom JARa na serwerze i odpal aplikację w przeglądarce